# hoodmood

Check out the mood of your hood! With hoodmood, you can gather data of a group of people by asking them questions.
For this project, we got a similar idea as kahoot! and therefore we tried to reconstruct it.

Project Requirements:

* JavaScript SPA Framework: React
* CSS Preprocessor: SCSS
* Interaction with an external service: Backend connection for storing user data, session data and for the sockets, which also use the backend service.
* Responsive: Every screen can be viewed on any device you like. Note: The so-called "Admin" screen is meant to be viewed on a big screen (--> beamer), because
  all the members are in a room, while doing a hoodmood session.
* Package Manager: yarn
* Module Bundler: webpack
* Task Runner: gulp
* Version Control: git
* socket.io: For the communication between Admin and Members, we are using Socket.io, with which we can build sessions, let member join sessions
  and communicate.

Link to our application, which is deployed on AWS:
http://hoodmood.at.s3-website.eu-central-1.amazonaws.com/

Link to our repository on Bitbucket:
https://Cyb3r16@bitbucket.org/Cyb3r16/hoodmood

Link to our video, which shows a screencast of the app´s UI flow:
https://www.dropbox.com/s/ob0ezc2z148tmyd/Hoodmood.mov?dl=0

In the video, you can see on the left side the Admin Screen and on the right side, you can see the Member Screen.

Installation for Frontend and Backend: (included in the zip-file on moodle)

* yarn install
* yarn run start
* Browser: localhost:3000
