import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';
import App from './components/app';
import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

import 'sanitize.css/sanitize.css';

import 'bootstrap/dist/css/bootstrap.css';
import './index.scss';

const target = document.querySelector('#root');

const fullscreen = {
  width: '100%',
  height: '100%'
};

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div style={fullscreen}>
        <AlertProvider template={AlertTemplate}>
          <App />
        </AlertProvider>
      </div>
    </ConnectedRouter>
  </Provider>,
  target
);
