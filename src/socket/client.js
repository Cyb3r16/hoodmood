import io from 'socket.io-client';

import store from '../store';
import { addUser } from '../redux/session';
import { BACKEND_URL } from '../redux/api';
import { receiveAnswer, toggleWaiting, sessionFinished } from '../redux/game';

var count = 0;

export const socket = io(BACKEND_URL); // setup a connection between the client and the server

export function receiveMessage() {
  socket.on('message', data => {
    return data;
  });
}

export function receiveMessageCallback(callback) {
  socket.on('message', data => {
    console.log('now in receiveMessageCallback... ');
    if (callback) callback(data);
    return data;
  });
}

export function sendMessage(msg) {
  try {
    socket.emit('message', msg);
  } catch (error) {
    console.error(`Error: ${error.message}`);
  }
}

export function joinRoom(roomNumber) {
  socket.emit('joinRoom', roomNumber);
}

export function joinSession(username) {
  socket.emit('addUser', username);
}

export function userJoinedSocket() {
  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('userJoined', data => {
    store.dispatch(addUser({ id: count, name: data.username }));
    count++;
  });
}

export function privateScreenSocketEvents() {
  socket.on('startWaiting', data => {
    setTimeout(() => {
      store.dispatch(toggleWaiting(true));
    }, 1000);
  });

  socket.on('endWaiting', data => {
    store.dispatch(toggleWaiting(false));
  });

  socket.on('endGameSession', data => {
    store.dispatch(sessionFinished(true));
  });
}

export function sendAnswer(answer) {
  socket.emit('sendAnswer', answer);
}

export function publicScreenSocketEvents() {
  socket.on('receiveAnswer', data => {
    store.dispatch(receiveAnswer(data));
  });
}

export function nextQuestionClient() {
  socket.emit('nextQuestion');
}

export function endSession() {
  socket.emit('endSession');
}
