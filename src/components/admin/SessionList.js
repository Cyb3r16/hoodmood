import React, { Component } from 'react';
import { PageHeader, ListGroup, ListGroupItem } from 'react-bootstrap';
import './SessionList.scss';

export default class SessionList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      sessions: []
    };
  }

  async componentDidMount() {
    if (!this.props.isAuthenticated) {
      return;
    }

    try {
      const sessions = await this.sessions();
      this.setState({ sessions });
    } catch (e) {
      alert(e);
    }

    this.setState({ isLoading: false });
  }

  handleSessionClick = event => {
    event.preventDefault();
    this.props.history.push(event.currentTarget.getAttribute('href'));
  };

  sessions() {
    return null;
    // return API.get("sessions", "/sessions");
  }

  renderSessionList(sessions) {
    return [{}].concat(sessions).map(
      (session, i) =>
        i !== 0 ? (
          <div class="flex-container">
            <ListGroupItem
              key={session.sessionId}
              href={`/sessions/${session.sessionId}`}
              onClick={this.handleSessionClick}
              header={session.content.trim().split('\n')[0]}>
              {'Created: ' + new Date(session.createdAt).toLocaleString()}
            </ListGroupItem>
          </div>
        ) : (
          <div className="flex-container-left add-session-field" key={session}>
            <ListGroupItem
              key="new"
              href="/sessions/new"
              onClick={this.handleSessionClick}>
              <h4>
                <b>{'\uFF0B'}</b> Create a new session
              </h4>
            </ListGroupItem>
          </div>
        )
    );
  }

  renderLander() {
    return (
      <div className="lander">
        <h1>Scratch</h1>
        <p>A simple note taking app</p>
      </div>
    );
  }

  renderSessions() {
    return (
      <div className="sessions">
        <PageHeader>Your Sessions</PageHeader>
        <ListGroup>{this.renderSessionList(this.state.sessions)}</ListGroup>
      </div>
    );
  }

  // <!-- {!this.state.isLoading && this.renderSessionList(this.state.sessions)}-->

  render() {
    return (
      <div className="SessionList">
        {!this.props.isAuthenticated
          ? this.renderSessions()
          : this.renderLander()}
      </div>
    );
  }

  // {this.props.isAuthenticated ? this.renderSessions() : this.renderLander()}
}
