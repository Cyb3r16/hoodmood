import React, { Component } from 'react';
import { FormControl, FormGroup } from 'react-bootstrap';
import LoaderButton from '../LoaderButton';
import { connect } from 'react-redux';
import { createSession } from '../../redux/session';
import { deleteError } from '../../redux/api';
import { withAlert } from 'react-alert';

class CreateSession extends Component {

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      triggerAnim: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.session && this.props.session.pin) this.props.history.push('/admin/questions-editor')

    if(nextProps.error !== this.props.error) {
      if(nextProps.error){
        this.props.deleteError();
        this.props.alert.show(nextProps.error.message, {type: 'error'});
      }
    }
  }

  validateForm() {
    return this.state.name.length > 0;
  }

  getValidationState(field) {
    switch (field) {
      case 'name':
        if (this.state.name.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'pin':
        if (this.state.pin.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      default:
        return null;
    }
  }

  shouldTriggerAnimation(field) {
    if (this.getValidationState(field) === 'error' && this.state.triggerAnim)
      return true;
    else
      return false;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.validateForm()) {
      this.setState({ triggerAnim: false });
      this.props.createSession({name: this.state.name})
    } else {
      this.setState({ triggerAnim: true });
      setTimeout(() => {
        this.setState({ triggerAnim: false });
      }, 500);
    }
  };

  render() {
    return (
      <div className="flex-container LoginMember">
        <form onSubmit={this.handleSubmit}>
          <p className="appFont fontStyle">Create Session</p>
          <div
            className={
              this.shouldTriggerAnimation('name') ? ' shake-horizontal shake-constant' : ''
            }>
            <FormGroup
              controlId="name"
              validationState={this.getValidationState('name')}
              bsSize="large">
              <FormControl
                autoFocus
                className="fullscreen input-field input-field-color"
                type="text"
                placeholder="Travelling,..."
                onChange={this.handleChange}
              />
              <FormControl.Feedback />
            </FormGroup>
          </div>
          <LoaderButton
            className="input-field submit-button-color submitButton"
            block
            bsSize="large"
            onChange={this.handleSubmit}
            type="submit"
            isLoading={this.props.isLoading}
            text="Create"
            loadingText="Creating..."
          />
        </form>
      </div>
    );
  }
};

export default connect(state => ({
  error: state.api.error,
  session: state.session.session,
  isLoading: state.api.isLoading
}), { createSession, deleteError })(withAlert(CreateSession));