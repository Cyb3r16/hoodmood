import React, { Component } from 'react';
import './PublicScreen.scss';

import { nextQuestion, receiveAnswer } from '../../redux/game';
import { connect } from 'react-redux';
import {
  publicScreenSocketEvents,
  nextQuestionClient,
  endSession
} from '../../socket/client';

class PublicScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seconds: 40,
      timerAnim: true
    };

    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  componentWillMount() {
    if (this.props.questions.length > 0) {
      publicScreenSocketEvents();
    } else {
      this.props.history.push('/sessions/new');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.questions[nextProps.count]) {
      this.stopTimer();
      endSession();
      this.props.history.push('/admin/statistic');
    }
  }

  componentDidMount() {
    this.setState({ seconds: 40 });
    this.startTimer();
  }

  startTimer() {
    this.timer = setInterval(this.countDown, 1000);
  }

  restartTimer() {
    clearInterval(this.timer);
    this.setState({ timerAnim: false });
    setTimeout(() => {
      this.setState({ timerAnim: true, seconds: 40 });
      this.startTimer();
    }, 1000);
  }

  stopTimer() {
    clearInterval(this.timer);
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      seconds: seconds
    });

    if (seconds === 0) {
      this.restartTimer();
      this.props.nextQuestion();
      nextQuestionClient();
    }
  }

  handleButtonClick(type) {
    switch (type) {
      case 'next':
        this.restartTimer();
        this.props.nextQuestion();
        nextQuestionClient();
        break;
      default:
        console.log('Unknown button click encountered...');
    }
  }

  render() {
    if (
      this.props.questions.length < 1 ||
      !this.props.questions[this.props.count]
    )
      return <div className="LoginMember" />;
    return (
      <div className="fullscreen">
        <div className="question flex-container">
          <p>{this.props.questions[this.props.count].question}</p>
        </div>
        <div className="flex-container">
          <div className="answer-counter">
            <p>{this.props.questionCount} Answers</p>
          </div>
          <button
            className="next-button btn-default"
            onClick={() => {
              this.handleButtonClick('next');
            }}
            type="button">
            Next
          </button>
        </div>

        <div className="grid-container grid-fullscreen">
          <div className="timer">
            <div className="wrapper">
              <div className="circle" />
              <div
                className={
                  'pie' +
                  (this.state.timerAnim ? ' spinner wrapper-border' : '')
                }
              />
              <div
                className={
                  'pie' + (this.state.timerAnim ? ' filler wrapper-border' : '')
                }
              />
              <div
                className={
                  'pie' + (this.state.timerAnim ? ' mask wrapper-border' : '')
                }
              />
              <div className="timer-img">
                <img src={require('../../assets/timer.png')} alt="Timer" />
              </div>
              <div className="timer-counter">{this.state.seconds}</div>
            </div>
          </div>
          <div className="grid-item red">
            <div
              className="answer fullscreen flex-container orientation-vertical"
              onClick={() => {
                this.handleButtonClick('A');
              }}>
              <img src={require('../../assets/triangle_black.png')} alt="A" />
              <p>{this.props.questions[this.props.count].answers.answerA}</p>
            </div>
          </div>
          <div className="grid-item blue ">
            <div
              className="answer fullscreen flex-container orientation-vertical"
              onClick={() => {
                this.handleButtonClick('B');
              }}>
              <img src={require('../../assets/hexagone_black.png')} alt="B" />
              <p>{this.props.questions[this.props.count].answers.answerB}</p>
            </div>
          </div>
          <div className="grid-item orange">
            <div
              className="answer fullscreen flex-container orientation-vertical"
              onClick={() => {
                this.handleButtonClick('C');
              }}>
              <img src={require('../../assets/circle_black.png')} alt="C" />
              <p>{this.props.questions[this.props.count].answers.answerC}</p>
            </div>
          </div>
          <div className="grid-item green">
            <div
              className="answer fullscreen flex-container orientation-vertical"
              onClick={() => {
                this.handleButtonClick('D');
              }}>
              <img src={require('../../assets/quadrat_black.png')} alt="D" />
              <p>{this.props.questions[this.props.count].answers.answerD}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    questions: state.game.questions,
    count: state.game.count,
    questionCount: state.game.questionCount
  }),
  { nextQuestion, receiveAnswer }
)(PublicScreen);
