import React  from 'react';
import './Statistic.scss';
import _ from 'underscore';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';

class Statistic extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEditorBoxVisible: false,
      itemClickedKey: undefined,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.answers[nextProps.count]) {
      this.props.history.push('/admin/CreateSession');
    }
  }

  getDatasetForIndex(idx) {
    var answerCounters = this.props.answers[idx];
    var data = {
      labels: ['answer A', 'answer B', 'answer C', 'answer D'],
      datasets: [
        {
          backgroundColor: ["rgba(235,103,15,0.5)", "rgb(255,166,2,0.5)","rgba(102,191,57,0.5)","rgba(51,204,204,0.5)" ],
          borderColor: ["rgba(235,103,15,1)", "rgb(255,166,2,1)","rgba(102,191,57,1)","rgba(51,204,204,1)" ],
          borderWidth: 2,
          hoverBackgroundColor: ["rgba(235,103,15,1)", "rgb(255,166,2,1)","rgba(102,191,57,1)","rgba(51,204,204,1)" ],
          hoverBorderColor: ["rgba(235,103,15,1)", "rgb(255,166,2,1)","rgba(102,191,57,1)","rgba(51,204,204,1)" ],
          data: [answerCounters.answerA, answerCounters.answerB, answerCounters.answerC, answerCounters.answerD]
        }
      ]
    };
    return data;
  }

  handleClick = (event, type, itemKey) => {
    event.preventDefault();

    switch (type ) {
      case 'questionItem' :
        if (this.state.itemClickedKey === itemKey) {
          this.setState({ itemClickedKey: undefined});
        } else {
          this.setState({ itemClickedKey: itemKey});
        }
        this.showQuestionAnswers(itemKey);
        break;
      default:
    }
  };

  showQuestionAnswers(key) {
    _.find(this.props.questions, function (obj) {
      return obj.key === key;
    });
  }

  renderStatistic = (itemKey, idx) => {
    return (
      <div>
        { (this.state.itemClickedKey === itemKey) && <div>
          <Bar
            data={this.getDatasetForIndex(idx)}
            width={100}
            height={50}
            options={{
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true,
                  }
                }]
              },
              legend: {
                display: false
              },
              maintainAspectRatio: true
            }}
          />
        </div> }
      </div>
    );
  }

  renderQuestionItem = (item, index) => {
    return (
      <div key={item.key}>
        <li key={item.key} onClick={(event) => this.handleClick(event, 'questionItem', item.key)}>
          <p>{item.question}</p>
          <ul>
            {this.renderStatistic(item.key, index)}
          </ul>
        </li>
      </div>
    );
  }

  render() {

    var questionItems = this.props.questions.map(this.renderQuestionItem);

    return (
      <div className="scrollable color-animation fullscreen flex-container orientation-vertical">
        <ul className="list-padding questionsList full-height custom-width">
          {questionItems}
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    questions: state.game.questions,
    answers: state.game.answers
  }
}

export default connect(mapStateToProps)(Statistic);