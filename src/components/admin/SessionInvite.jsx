import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col, Grid, Row } from 'react-bootstrap';
import FlipMove from 'react-flip-move';
import { addUser } from '../../redux/session';
import { joinRoom, nextQuestionClient, userJoinedSocket } from '../../socket/client';
import LoaderButton from '../LoaderButton';
var QRCode = require('qrcode-react');


class SessionInvite extends Component {


  componentWillMount() {
    if(!this.props.session) this.props.history.push('/sessions/new')
  }

  componentDidMount() {
    //this.addUserToList();
    if(this.props.session) {
      joinRoom(this.props.session.pin)
      userJoinedSocket();
    }
  }

  addUserToList() {
    let self = this;
    var counter = 0;
    setInterval(function(){
      self.props.addUser({ id: counter, name: 'Testi ' + counter});
      counter++;
    }, 500);

  }

  render() {

    var listItems = this.props.userList.map(this.createTasks);

    return (
      <Grid fluid={true} className="LoginMember">
        <Row className="scrollable fullscreen">
          <Col md={9} className="flex-container-center-v-h full-height">
            <p className="appFont fontStyle">Join Session</p>
            <QRCode value={this.props.session && this.props.session.pin ? 'http://hoodmood.at.s3-website.eu-central-1.amazonaws.com/login-member/' + this.props.session.pin : ''} />
            <p className="appFont fontStyle game-pin">{this.props.session && this.props.session.pin}</p>
            <LoaderButton
              style={{width: '300px'}}
              className="input-field submit-button-color"
              block
              bsSize="large"
              onClick={() => {
                this.props.history.push('/admin/public-screen')
                nextQuestionClient();
              }}
              text="Check the mood in your hood!"
              loadingText="hoodie moodie..."
            />
          </Col>
          <Col md={3} className="flex-container-center-v-h member-list full-height">
            <p style={{color: 'white'}} className="appfont">User List:</p>
            <div style={{ padding: '20px', height: '85%', width: '80%', overflowY: 'scroll', color: 'white', border: '4px dotted white', display: 'flex', justifyContent: 'center'}}>
              <FlipMove duration={250} easing="ease-out">
                {listItems}
              </FlipMove>
            </div>
          </Col>
        </Row>
      </Grid>
    )
  }

  createTasks(item) {
    return <div key={item.id}>{item.name}</div>;
  }

};

export default connect(state => ({
  user: state.api.user,
  session: state.session.session,
  userList: state.session.user
}), { addUser })(SessionInvite);