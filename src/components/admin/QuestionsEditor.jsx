import React  from 'react';
import './QuestionsEditor.scss';
import { FormGroup, FormControl } from 'react-bootstrap';
import LoaderButton from '../LoaderButton';
import FlipMove from "react-flip-move";
import _ from 'underscore';
import { connect } from 'react-redux';
import { addQuestion } from '../../redux/game';
import { joinRoom, joinSession } from '../../socket/client';

class QuestionsEditor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      session: undefined,
      questions: [],
      newQuestion: '',
      newAnswers: {},
      triggerAnim: false,
      isEditorBoxVisible: false,
      itemClickedKey: undefined,
      rotate: false
    };

    this.addQuestion = this.addQuestion.bind(this);
    this.deleteQuestion = this.deleteQuestion.bind(this);

  }

  validateForm() {
    return this.state.newQuestion.length > 0
      && this.getValidationState('answerA') === 'success'
      && this.getValidationState('answerB') === 'success'
      && this.getValidationState('answerC') === 'success'
      && this.getValidationState('answerD') === 'success';
  }

  getValidationState(field) {
    switch (field) {
      case 'newQuestion':
        if (this.state.newQuestion.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'answerA':
        if( this.state.newAnswers.answerA !== undefined && this.state.newAnswers.answerA.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'answerB':
        if( this.state.newAnswers.answerB !== undefined && this.state.newAnswers.answerB.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'answerC':
        if( this.state.newAnswers.answerC !== undefined && this.state.newAnswers.answerC.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'answerD':
        if( this.state.newAnswers.answerD !== undefined && this.state.newAnswers.answerD.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      default:
        return null;
    }
  }

  shouldTriggerAnimation(field) {
    if (this.getValidationState(field) === 'error' && this.state.triggerAnim)
      return true;
    else
      return false;
  }

  handleClick = (event, type, itemKey) => {
    event.preventDefault();

    switch (type ) {
      case 'questionItem' :
        if (this.state.itemClickedKey === itemKey) {
          this.setState({ itemClickedKey: undefined});
        } else {
          this.setState({ itemClickedKey: itemKey});
        }
        this.showQuestionAnswers(itemKey);
        break;
      case 'addQuestion' :
        this.resetQuestionEditorForm();
        this.setState({ rotate: true });
        setTimeout(() => {
          this.setState({ rotate: false });
        }, 500);
        this.setState({ isEditorBoxVisible: !this.state.isEditorBoxVisible });
        break;
      default:
    }
  };

  handleChange = event => {
    switch(event.target.id) {
      case 'newQuestion':
        this.setState({
          newQuestion: event.target.value
        });
        break;
      case 'answerA':
        this.setState({
          newAnswers: { ...this.state.newAnswers, 'answerA': event.target.value}
        });
        break;
      case 'answerB':
        this.setState({
          newAnswers: {...this.state.newAnswers, 'answerB': event.target.value}
        });
        break;
      case 'answerC':
        this.setState({
          newAnswers: {...this.state.newAnswers, 'answerC': event.target.value}
        });
        break;
      case 'answerD':
        this.setState({
          newAnswers: {...this.state.newAnswers, 'answerD': event.target.value}
        });
        break;
      default:
        console.log("Unknown event encountered in handleChange...");
    }
  };

  handleSubmit = (event, type) => {
    event.preventDefault();

    if (this.validateForm()) {
      this.setState({ triggerAnim: false });
      this.setState({ isLoading: true });
      joinRoom(this.state.pin);
      joinSession(this.state.name);
    } else {
      this.setState({ triggerAnim: true });
      setTimeout(() => {
        this.setState({ triggerAnim: false });
      }, 500);
    }
  };


  addQuestion = event => {
    event.preventDefault();

    if (this.validateForm()) {
      this.setState({ triggerAnim: false });
      var newQuestionPack = {
        question: this.state.newQuestion,
        answers: this.state.newAnswers,
        key: Date.now()
      };

      this.setState({ isEditorBoxVisible: false });

      setTimeout(() => {
        this.setState((prevState) => {
          return {
            questions: prevState.questions.concat(newQuestionPack)
          };
        });
      }, 300);
      this.resetQuestionEditorForm();
    } else {
      this.setState({ triggerAnim: true });
      setTimeout(() => {
        this.setState({ triggerAnim: false });
      }, 500);
    }
  };

  deleteQuestion(key) {
    var filteredQuestions = this.state.questions.filter(function (item) {
      return (item.key !== key);
    });
    this.setState({ questions: filteredQuestions });
  }

  showQuestionAnswers(key) {
    var obj = _.find(this.state.questions, function (obj) { return obj.key === key; });
    console.log("showQuestionAnswers: ", obj.answers);
  }

  resetQuestionEditorForm() {
    this.setState({ newQuestion: ''});
    this.setState({ newAnswers: {} });
  }

  renderQuestionAnswers = (itemKey, answer) => {
    return (
      <div>
        { (this.state.itemClickedKey === itemKey) && <div className="answers">
          <li key="answerA">
            <p>{answer.answerA}</p>
          </li>
          <li key="answerB">
            <p>{answer.answerB}</p>
          </li>
          <li key="answerC">
            <p>{answer.answerC}</p>
          </li>
          <li key="answerD">
            <p>{answer.answerD}</p>
          </li>
        </div> }
      </div>
    );
  }

  renderQuestionItem = item => {
    var questionObj = _.find(this.state.questions, function (obj) {
      if (obj.key === item.key) {
        return item.answers;
      }
      return null;
    });
    return (
      <div key={item.key}>
        <li key={item.key} onClick={(event) => this.handleClick(event, 'questionItem', item.key)}>
          <p>{item.question}</p>
          <div className="flex-container delete-btn" onClick={() => this.deleteQuestion(item.key)}>
            <img src={require('../../assets/dustbin.png')} alt="dustbin" />
          </div>
          <ul>
            {this.renderQuestionAnswers(item.key, questionObj.answers)}
          </ul>
        </li>
      </div>
    );
  }

  render() {

    var questionItems = this.state.questions.map(this.renderQuestionItem);

    return (
      <div className="color-animation fullscreen flex-container orientation-vertical">
        <button
          onClick={(event) => this.handleClick(event,'addQuestion')}
          className={"add-question flex-container orientation-vertical" +
          (this.state.rotate ? " rotate" : "")}>
          <img src={
            !this.state.isEditorBoxVisible
              ? require('../../assets/add_icon.png')
              : require('../../assets/close_x.png')
          } alt="addQuestion" />
          <p>Add question</p>
        </button>

        <FlipMove style={{width: "100%"}} duration={250} easing="ease-out">
          {this.state.isEditorBoxVisible && <form className="full-width" onSubmit={this.addQuestion}>
            <div className="flex-container orientation-vertical">
              <div
                className={"custom-width" +
                (this.shouldTriggerAnimation('newQuestion') ? ' shake-horizontal shake-constant' : '')
                }>
                <FormGroup
                  controlId="newQuestion"
                  validationState={this.getValidationState('newQuestion')}
                  bsSize="large">
                  <FormControl
                    className="fullscreen input-field input-field-color"
                    type="text"
                    placeholder="Question"
                    onChange={this.handleChange}
                  />
                  <FormControl.Feedback />
                </FormGroup>
              </div>

              <div className="grid-container custom-width">
                <div className="grid-item margin-right">
                  <div
                    className={"full-width" +
                    (this.shouldTriggerAnimation('answerA') ? ' shake-horizontal shake-constant' : '')
                    }>
                    <FormGroup
                      controlId="answerA"
                      validationState={this.getValidationState('answerA')}
                      bsSize="large">
                      <FormControl
                        className="fullscreen input-field input-field-color"
                        type="text"
                        placeholder="Answer A"
                        onChange={this.handleChange}
                      />
                      <FormControl.Feedback />
                    </FormGroup>
                  </div>
                </div>
                <div className="grid-item margin-left">
                  <div
                    className={"full-width" +
                    (this.shouldTriggerAnimation('answerB') ? ' shake-horizontal shake-constant' : '')
                    }>
                    <FormGroup
                      controlId="answerB"
                      validationState={this.getValidationState('answerB')}
                      bsSize="large">
                      <FormControl
                        className="fullscreen input-field input-field-color"
                        type="text"
                        placeholder="Answer B"
                        onChange={this.handleChange}
                      />
                      <FormControl.Feedback />
                    </FormGroup>
                  </div>
                </div>
                <div className="grid-item margin-right">
                  <div
                    className={"full-width" +
                    (this.shouldTriggerAnimation('answerC') ? ' shake-horizontal shake-constant' : '')
                    }>
                    <FormGroup
                      controlId="answerC"
                      validationState={this.getValidationState('answerC')}
                      bsSize="large">
                      <FormControl
                        className="fullscreen input-field input-field-color"
                        type="text"
                        placeholder="Answer C"
                        onChange={this.handleChange}
                      />
                      <FormControl.Feedback />
                    </FormGroup>
                  </div>
                </div>
                <div className="grid-item margin-left">
                  <div
                    className={"full-width" +
                    (this.shouldTriggerAnimation('answerD') ? ' shake-horizontal shake-constant' : '')
                    }>
                    <FormGroup
                      controlId="answerD"
                      validationState={this.getValidationState('answerD')}
                      bsSize="large">
                      <FormControl
                        className="fullscreen input-field input-field-color"
                        type="text"
                        placeholder="Answer D"
                        onChange={this.handleChange}
                      />
                      <FormControl.Feedback />
                    </FormGroup>
                  </div>
                </div>
              </div>

            </div>
            <div className="flex-container">
              <div className="custom-width">
                <LoaderButton
                  className="input-field submit-button-color"
                  block
                  bsSize="large"
                  type="submit"
                  isLoading={this.state.isLoading}
                  text="Add question"
                  loadingText="Adding..."
                />
              </div>
            </div>
          </form> }
        </FlipMove>

        { !this.state.isEditorBoxVisible && <ul className="questionsList custom-width">
          <FlipMove duration={250} easing="ease-out">
            {questionItems}
          </FlipMove>
        </ul>}

        { (!this.state.isEditorBoxVisible && this.state.questions.length > 0) && <div className="flex-container custom-width">
          <LoaderButton
            className="input-field submit-button-color"
            block
            bsSize="large"
            onClick={() => {
              this.props.addQuestion(this.state.questions)
              this.props.history.push('/admin/invite')
            }}
            isLoading={this.state.isLoading}
            text="Check the mood in your hood!"
            loadingText="hoodie moodie..."
          />
        </div> }

      </div>

    );
  }
}

// mapsStateToProps, mapsActionsToProbs
export default connect(state => ({
  questions: state.game.questions
}), {addQuestion})(QuestionsEditor)