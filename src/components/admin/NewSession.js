import React, { Component } from 'react';
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import ReactTable from 'react-table';
import LoaderButton from '../LoaderButton';
import './NewSession.scss';
import '../login/LoginMember.scss';

export default class NewSession extends Component {
  constructor(props) {
    super(props);

    this.state = {
      perro: 'form-control',
      input: '+',
      question: '',
      header: 'Create new session',
      isLoading: false,
      title: '',
      questionContainer: '',
      questions: []
    };
  }

  validateForm() {
    return this.state.title.length > 0 && this.state.content.length;
  }

  getValidationState(field) {
    switch (field) {
      case 'title':
        if (this.state.title.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      default:
        return null;
    }
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });
  };

  onSubmit(e) {
    e.preventDefault();
    if (this.submitInput.value == '+') {
      const obj = { question: this.state.question };
      this.setState({
        questions: [...this.state.questions, obj],
        question: ''
      });
    } else if (this.submitInput.value == 'edit') {
      const pos = Number(this.rowRef.value);
      this.state.questions[pos].question = this.inputQuestion.value;
      this.submitInput.value = '+';
      this.clearInputs();

      this.forceUpdate();
    }
  }

  clearInputs() {
    this.inputQuestion.value = '';
    this.state.question = '';
  }

  deleteRow(row) {
    let pos = this.state.questions.indexOf(row);
    //const newlist = [].concat(questions)
    //newlist.splice(pos, 1);
    //this.setState({questions: newlist});
    this.setState({
      questions: this.state.questions.splice(this.state.questions, pos)
    });
    this.clearInputs();
  }
  editRow(row) {
    let pos = this.state.questions.indexOf(row);
    this.inputQuestion.value = this.state.questions[row].question;
    this.state.question = this.state.questions[row].question;
    this.rowRef.value = row;
    this.state.input = 'edit';
    this.forceUpdate();
  }

  render() {
    return (
      <div className="flex-container NewSession">
        <form onSubmit={this.handleSubmit}>
          <p className="appFont">{this.state.header}</p>
          <FormGroup
            controlId="newSession"
            validationState={this.getValidationState('title')}
            bsSize="large">
            <FormControl
              autoFocus
              className="fullscreen input-field input-field-color"
              type="text"
              placeholder="Session Title"
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup
            controlId="newQuestion"
            validationState={this.getValidationState('question')}
            bsSize="large"
            onSubmit={this.onSubmit.bind(this)}>
            <div className="button">
              <FormControl
                autoFocus
                className="fullscreen input-field input-field-color"
                type="text"
                placeholder="New Question"
                onChange={event =>
                  this.setState({ question: event.target.value })
                }
                value={this.state.question}
                required
                ref={ref => (this.inputQuestion = ref)}
              />

              <button
                controlId="submitInput"
                onClick={this.onSubmit.bind(this)}
                ref={ref => (this.submitInput = ref)}
                value={this.state.input}
                className="btn btn-default button">
                +
              </button>
            </div>

            <input
              type="hidden"
              className="row-ref"
              value=""
              ref={ref => (this.rowRef = ref)}
            />

            <div className="fullscreen table-field input-field-color">
              <table>
                <tbody
                  style={{
                    height: '300px',
                    overflow: 'scroll',
                    display: 'block'
                  }}
                  aria-placeholder="No question added">
                  {this.state.questions.map((data, index) => {
                    return (
                      <Row
                        editRow={this.editRow.bind(this)}
                        questions={this.state.questions}
                        data={data}
                        key={index}
                        row={index}
                        deleteRow={this.deleteRow.bind(this)}
                      />
                    );
                  })}
                </tbody>
              </table>
            </div>
          </FormGroup>

          <LoaderButton
            block
            bsStyle="primary"
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Create"
            loadingText="Creating…"
          />
        </form>
      </div>
    );
  }
}

class Row extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <tr>
          <td>{this.props.data.question}</td>
          <td
            onClick={() => {
              this.props.deleteRow(this.props.row);
            }}>
            <a href="#">
              <span className="glyphicon glyphicon-remove" />
            </a>
          </td>
        </tr>
      </div>
    );
  }
}
