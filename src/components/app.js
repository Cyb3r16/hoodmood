import React from 'react';
import { Route, Switch } from 'react-router-dom';
import LoginMember from './login/LoginMember';
import RegisterAdmin from './login/RegisterAdmin';
import SessionList from './admin/SessionList';
import PublicScreen from './admin/PublicScreen';
import Header from './Header';
import CreateSession from './admin/CreateSession';
import QuestionsEditor from './admin/QuestionsEditor';
import SessionInvite from './admin/SessionInvite';
import PrivateScreen from './member/PrivateScreen';
import Statistic from './admin/Statistic';

const App = () => (
  <div style={{ width: '100%', height: '100%' }}>
    <header className={'header'}>
      <Header />
    </header>

    <main style={{ width: '100%', height: '100%' }}>
      <Switch>
        <Route exact path="/" component={LoginMember} />
        <Route exact path="/login-member/:pin" component={LoginMember} />
        <Route exact path="/register-admin" component={RegisterAdmin} />
        <Route exact path="/login-admin" component={RegisterAdmin} />
        <Route exact path="/sessions/new" component={CreateSession} />
        <Route exact path="/sessions" component={SessionList} />
        <Route exact path="/member/private-screen" component={PrivateScreen} />
        <Route exact path="/admin/public-screen" component={PublicScreen} />
        <Route
          exact
          path="/admin/questions-editor"
          component={QuestionsEditor}
        />
        <Route exact path="/admin/invite" component={SessionInvite} />
        <Route exact path="/admin/statistic" component={Statistic} />
        <Route component={LoginMember} />
      </Switch>
    </main>
  </div>
);

export default App;
