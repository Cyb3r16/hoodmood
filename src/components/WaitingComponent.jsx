import React, { Component } from 'react';
import { ClimbingBoxLoader } from 'react-spinners';

class WaitingComponent extends Component {

  render() {
    return (
      <div className="flex-container-center-v-h LoginMember">
        <p className="appFont">Waiting for other players</p>
        <ClimbingBoxLoader
          color={'white'}
        />
      </div>
    )

  }
};

export default WaitingComponent;