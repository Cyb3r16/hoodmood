import React, { Component } from 'react';
import './PrivateScreen.scss';
import { privateScreenSocketEvents, sendAnswer } from '../../socket/client';
import { connect } from 'react-redux';
import WaitingComponent from '../WaitingComponent';

class PrivateScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      topic: 'Countries?',
      newIdea: '',
      isLoading: false,
      response: false,
      selectedAnswer: '',
      selectedAnswerAnim: ''
    };
  }

  componentWillMount() {
    if(this.props.gamePin){
      privateScreenSocketEvents();
    } else {
      this.props.history.push('/');
    }
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.sessionFinished) this.props.history.push('/');
  }

  validateForm() {
    return this.state.newIdea.length > 0;
  }

  handleButtonClick(type) {

    sendAnswer({ propt: type });

    this.setState({ selectedAnswer: type });
    this.setState({ selectedAnswerAnim: type });
    setTimeout(() => {
      this.setState({ selectedAnswer: undefined });
      this.setState({ selectedAnswerAnim: undefined });
    }, 1000);
  }

  render() {
    if(this.props.waiting) {
      return <WaitingComponent/>
    }
    return (
      <div className="fullscreen">
        <div className="grid-container fullscreen">
          <div className="grid-item red">
            <div
              className={"answer fullscreen flex-container orientation-vertical" +
                ((this.state.selectedAnswer === 'answerA') ? ' answerSelected' : '')
              }
              onClick={() => {
                this.handleButtonClick('answerA');
              }}>
              <img
                className={((this.state.selectedAnswerAnim === 'answerA') ? ' answerSelectedAnim' : '')}
                src={require('../../assets/triangle_black.png')} alt="A" />
            </div>
          </div>
          <div className="grid-item blue ">
            <div
              className={"answer fullscreen flex-container orientation-vertical" +
              ((this.state.selectedAnswer === 'answerB') ? ' answerSelected' : '')
              }
              onClick={() => {
                this.handleButtonClick('answerB');
              }}>
              <img
                className={((this.state.selectedAnswerAnim === 'answerB') ? ' answerSelectedAnim' : '')}
                src={require('../../assets/hexagone_black.png')} alt="B" />
            </div>
          </div>
          <div className="grid-item orange">
            <div
              className={"answer fullscreen flex-container orientation-vertical" +
              ((this.state.selectedAnswer === 'answerC') ? ' answerSelected' : '')
              }
              onClick={() => {
                this.handleButtonClick('answerC');
              }}>
              <img
                className={((this.state.selectedAnswerAnim === 'answerC') ? ' answerSelectedAnim' : '')}
                src={require('../../assets/circle_black.png')} alt="C" />
            </div>
          </div>
          <div className="grid-item green">
            <div
              className={"answer fullscreen flex-container orientation-vertical" +
              ((this.state.selectedAnswer === 'answerD') ? ' answerSelected' : '')
              }
              onClick={() => {
                this.handleButtonClick('answerD');
              }}>
              <img
                className={((this.state.selectedAnswerAnim === 'answerD') ? ' answerSelectedAnim' : '')}
                src={require('../../assets/quadrat_black.png')} alt="D" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    waiting: state.game.waiting,  // definiert in den props eine Variable die "waiting" heißt und state von waiting wird zugewiesen
    // game referenziert auf den reducer, der waiting enthält
    gamePin: state.game.gamePin,
    sessionFinished: state.game.sessionFinished
  }
}

export default connect(mapStateToProps)(PrivateScreen);