import React, { Component } from 'react';
import { FormGroup, FormControl } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import LoaderButton from '../LoaderButton';
import { joinRoom, joinSession } from '../../socket/client';
import { setGamePin } from '../../redux/game';
import { connect } from 'react-redux';
import { checkGamePin } from '../../redux/session';
import { withAlert } from 'react-alert';
import { deleteError } from '../../redux/api';

class LoginMember extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      pin: '',
      triggerAnim: false
    };
  }

  componentWillMount() {
    if (
      this.props.match &&
      this.props.match.params &&
      this.props.match.params.pin
    ) {
      this.setState({ pin: this.props.match.params.pin });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.session && this.props.session !== nextProps.session) {
      this.props.setGamePin(this.state.pin);
      joinRoom(this.state.pin);
      joinSession(this.state.name);
      this.props.history.push('/member/private-screen');
    }
    if (nextProps.error !== this.props.error) {
      if (
        nextProps.error &&
        !nextProps.error.message.includes('You must be logged in')
      ) {
        this.props.deleteError();
        this.props.alert.show(nextProps.error.message, { type: 'error' });
      }
    }
  }

  validateForm() {
    return this.state.name.length > 0 && this.state.pin.length > 0;
  }

  getValidationState(field) {
    switch (field) {
      case 'name':
        if (this.state.name.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'pin':
        if (this.state.pin.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      default:
        return null;
    }
  }

  shouldTriggerAnimation(field) {
    if (this.getValidationState(field) === 'error' && this.state.triggerAnim)
      return true;
    else return false;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.validateForm()) {
      this.props.checkGamePin({ gamePin: this.state.pin });
      this.setState({ triggerAnim: false });
    } else {
      this.setState({ triggerAnim: true });
      setTimeout(() => {
        this.setState({ triggerAnim: false });
      }, 500);
    }
  };

  render() {
    return (
      <div className="flex-container LoginMember">
        <form onSubmit={this.handleSubmit}>
          <p className="logoFont flex-container">hoodmood</p>
          <div
            className={
              this.shouldTriggerAnimation('name')
                ? ' shake-horizontal shake-constant'
                : ''
            }>
            <FormGroup
              controlId="name"
              validationState={this.getValidationState('name')}
              bsSize="large">
              <FormControl
                autoFocus
                className="fullscreen input-field input-field-color"
                type="text"
                placeholder="Enter your Name"
                onChange={this.handleChange}
              />
              <FormControl.Feedback />
            </FormGroup>
          </div>
          <div
            className={
              this.shouldTriggerAnimation('pin')
                ? ' shake-horizontal shake-constant'
                : ''
            }>
            <FormGroup
              controlId="pin"
              validationState={this.getValidationState('pin')}
              bsSize="large">
              <FormControl
                className="fullscreen input-field input-field-color"
                type="text"
                placeholder="PIN"
                onChange={this.handleChange}
                value={this.state.pin}
              />
              <FormControl.Feedback />
            </FormGroup>
          </div>
          <LoaderButton
            className="input-field submit-button-color submitButton"
            block
            bsSize="large"
            onChange={this.handleSubmit}
            type="submit"
            isLoading={this.props.isLoading}
            text="Connect"
            loadingText="Connecting..."
          />
          <FormGroup className="flex-container no-margin-bottom">
            <p className="login-reference">No account yet? </p>
            <Link to="/register-admin">Register!</Link>
          </FormGroup>
          <FormGroup className="flex-container">
            <p className="login-reference">Already have an account? </p>
            <Link to="/login-admin">Log in!</Link>
          </FormGroup>
        </form>
      </div>
    );
  }
}

export default connect(
  state => ({
    isLoading: state.api.isLoading,
    error: state.api.error,
    session: state.session.session
  }),
  { setGamePin, checkGamePin, deleteError }
)(withAlert(LoginMember));
