import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createUser, login } from '../../redux/user';
import { Link } from 'react-router-dom';
import { FormGroup, FormControl, Checkbox } from 'react-bootstrap';
import LoaderButton from '../LoaderButton';
import { withAlert } from 'react-alert';
import { deleteError } from '../../redux/api';

class RegisterAdmin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      password: '',
      confirmPassword: '',
      triggerAnim: false,
      rememberMe: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.user) this.props.history.push('/sessions/new');

    if (nextProps.error !== this.props.error) {
      if (
        nextProps.error &&
        !nextProps.error.message.includes('You must be logged in')
      ) {
        this.props.deleteError();
        this.props.alert.show(nextProps.error.message, { type: 'error' });
      }
    }
  }

  validateForm() {
    if (this.props.location.pathname === '/register-admin')
      return (
        this.state.name.length > 0 &&
        this.state.email.length > 0 &&
        this.state.password.length > 0 &&
        this.state.password === this.state.confirmPassword
      );
    else {
      return this.state.email.length > 0 && this.state.password.length > 0;
    }
  }

  getValidationState(field) {
    switch (field) {
      case 'name':
        if (this.state.name.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'email':
        if (this.state.email.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'password':
        if (this.state.password.length > 0) {
          return 'success';
        } else {
          return 'error';
        }
      case 'confirmPassword':
        if (this.state.confirmPassword === this.state.password) {
          return 'success';
        } else {
          return 'error';
        }
      default:
        return null;
    }
  }

  shouldTriggerAnimation(field) {
    if (this.getValidationState(field) === 'error' && this.state.triggerAnim)
      return true;
    else return false;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = async event => {
    event.preventDefault();

    if (this.validateForm()) {
      this.setState({ triggerAnim: false });

      if (this.props.location.pathname === '/register-admin') {
        this.props.createUser({
          name: this.state.name,
          email: this.state.email,
          password: this.state.password
        });
      } else {
        this.props.login({
          email: this.state.email,
          password: this.state.password,
          rememberMe: this.state.rememberMe
        });
      }
    } else {
      this.setState({ triggerAnim: true });
      setTimeout(() => {
        this.setState({ triggerAnim: false });
      }, 500);
    }
  };

  renderForm() {
    return (
      <div className="flex-container RegisterAdmin">
        <form onSubmit={this.handleSubmit}>
          <p className="logoFont flex-container">hoodmood</p>
          {this.props.location.pathname === '/register-admin' && (
            <div
              className={
                this.shouldTriggerAnimation('name')
                  ? ' shake-horizontal shake-constant'
                  : ''
              }>
              <FormGroup
                controlId="name"
                validationState={this.getValidationState('name')}
                bsSize="large">
                <FormControl
                  className="fullscreen input-field input-field-color"
                  autoFocus
                  type="text"
                  placeholder="Name"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
              </FormGroup>
            </div>
          )}
          <div
            className={
              this.shouldTriggerAnimation('email')
                ? ' shake-horizontal shake-constant'
                : ''
            }>
            <FormGroup
              controlId="email"
              validationState={this.getValidationState('email')}
              bsSize="large">
              <FormControl
                className="fullscreen input-field input-field-color"
                type="email"
                placeholder="Email"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <FormControl.Feedback />
            </FormGroup>
          </div>
          <div
            className={
              this.shouldTriggerAnimation('password')
                ? ' shake-horizontal shake-constant'
                : ''
            }>
            <FormGroup
              controlId="password"
              validationState={this.getValidationState('password')}
              bsSize="large">
              <FormControl
                className="fullscreen input-field input-field-color"
                value={this.state.password}
                onChange={this.handleChange}
                type="password"
                placeholder="Password"
              />
              <FormControl.Feedback />
            </FormGroup>
          </div>
          {this.props.location.pathname === '/register-admin' && (
            <div
              className={
                this.shouldTriggerAnimation('confirmPassword')
                  ? ' shake-horizontal shake-constant'
                  : ''
              }>
              <FormGroup
                controlId="confirmPassword"
                validationState={this.getValidationState('confirmPassword')}
                bsSize="large">
                <FormControl
                  className="fullscreen input-field input-field-color"
                  value={this.state.confirmPassword}
                  onChange={this.handleChange}
                  type="password"
                  placeholder="Confirm Password"
                />
                <FormControl.Feedback />
              </FormGroup>
            </div>
          )}
          {this.props.location.pathname === '/login-admin' && (
            <FormGroup className="flex-container">
              <Checkbox
                className="checkbox"
                onChange={evt =>
                  this.setState({ rememberMe: evt.target.checked })
                }>
                Remember me
              </Checkbox>
            </FormGroup>
          )}
          <LoaderButton
            className="input-field submit-button-color submitButton"
            block
            bsSize="large"
            type="submit"
            isLoading={this.props.isLoading}
            text={
              this.props.location.pathname === '/register-admin'
                ? 'Signup'
                : 'Login'
            }
            loadingText={
              this.props.location.pathname === '/register-admin'
                ? 'Signing up...'
                : 'Logging in...'
            }
          />
          {this.props.location.pathname === '/register-admin' && (
            <FormGroup className="flex-container">
              <p className="login-reference">Already have an account? </p>
              <Link to="/login-admin">Log in!</Link>
            </FormGroup>
          )}
          {this.props.location.pathname === '/login-admin' && (
            <FormGroup className="flex-container">
              <p className="login-reference">No account yet? </p>
              <Link to="/register-admin">Register!</Link>
            </FormGroup>
          )}
          <FormGroup className="flex-container">
            <p className="login-reference">A Guest? </p>
            <Link to="/login-member">Play!</Link>
          </FormGroup>
        </form>
      </div>
    );
  }

  render() {
    return <div className="RegisterAdmin">{this.renderForm()}</div>;
  }
}

RegisterAdmin.propTypes = {
  createUser: PropTypes.func,
  login: PropTypes.func
};

export default connect(
  state => ({
    error: state.api.error,
    user: state.api.user,
    isLoading: state.api.isLoading
  }),
  { createUser, login, deleteError }
)(withAlert(RegisterAdmin));
