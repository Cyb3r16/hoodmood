import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { logout, tryToLogin } from '../redux/user';

class Header extends Component {
  constructor(props) {
    super(props);
    this.props.tryToLogin();
  }

  render() {
    return (
      <div>
        <Link to="/">Home </Link>
        { !this.props.user &&<Link to="/register-admin">Register </Link> }
        { !this.props.user &&<Link to="/login-admin">Login </Link> }
        { this.props.user && <Link to="/sessions/new">New Session </Link> }
        { this.props.user && <Link to="/" onClick={(e) => this.props.logout()}>Logout </Link>}
      </div>
    )
  }
}

export default connect(state => ({
  user: state.api.user
}), { tryToLogin, logout })(Header);