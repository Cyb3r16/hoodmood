import Rx from 'rxjs/Rx';
import { createAction, handleActions } from 'redux-actions';
import { combineEpics } from 'redux-observable';

export const BACKEND_URL = 'http://localhost:8081';

const request = createAction('api/REQUEST');
const error = createAction('api/ERROR');
const done = createAction('api/DONE');

export const receiveUser = createAction('api/RECEIVE_USER');
export const logoutUser = createAction('api/LOGOUT_USER');
export const deleteError = createAction('api/DELETE_ERROR');

export default (...args) => {
  let state = handleActions(
    {
      [request](state) {
        return { ...state, openRequests: state.openRequests + 1 };
      },
      [done](state) {
        return { ...state, openRequests: state.openRequests - 1 };
      },
      [receiveUser](state, { payload }) {
        return { ...state, user: payload };
      },
      [logoutUser](state, { payload }) {
        return { ...state, user: undefined };
      },
      [error](state, { payload }) {
        return { ...state, error: payload };
      },
      [deleteError](state) {
        return { ...state, error: undefined };
      }
    },
    {
      isLoading: false,
      openRequests: 0,
      user: undefined
    }
  )(...args);
  return { ...state, isLoading: state.openRequests > 0 };
};

const epic$ = new Rx.BehaviorSubject(combineEpics());

export const epic = (action$, store) =>
  epic$.mergeMap(epic => epic(action$, store));

const ajax = req =>
  new Promise((resolve, reject) =>
    epic$.next(() =>
      ajax$(req, obs =>
        obs
          .map(resolve)
          .ignoreElements()
          .catch(err => {
            reject(err);
            throw err;
          })
      )
    )
  );

const ajax$ = ({ method, path, body, headers, responseType }, obs) =>
  Rx.Observable.concat(
    Rx.Observable.of(request(`${method} ${path}`)),
    Rx.Observable.ajax({
      method,
      url: BACKEND_URL + path,
      body,
      headers: {
        ...headers
      },
      withCredentials: true,
      credentials: 'include',
      responseType
    })
      .let(obs)
      .catch(err => Rx.Observable.of(error(err.response))),
    Rx.Observable.of(done(`${method} ${path}`))
  );

export const get$ = ({ path, headers, responseType = 'json' }, obs) =>
  ajax$({ method: 'GET', path, headers, responseType }, obs);

export const get = ({ path, headers, responseType = 'json' }) =>
  ajax({ method: 'GET', path, headers, responseType });

export const getJSON$ = ({ path, headers }, obs) =>
  ajax$({ method: 'GET', path, headers, responseType: 'json' }, o =>
    o.pluck('response').let(obs)
  );

export const getJSON = ({ path, headers }) =>
  ajax({ method: 'GET', path, headers, responseType: 'json' }).then(
    res => res.response
  );

export const post$ = ({ path, headers, body, responseType = 'json' }, obs) =>
  ajax$({ method: 'POST', path, headers, body, responseType }, obs);

export const post = ({ path, headers, body, responseType = 'json' }) =>
  ajax({ method: 'POST', path, headers, body, responseType });
