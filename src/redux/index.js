import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable';
import { routerReducer } from 'react-router-redux';
import api, { epic as apiEpic } from './api';
import { epic as userEpic } from './user';
import session, { epic as sessionEpic } from './session';
import game from './game';

export const epic = combineEpics(userEpic, apiEpic, sessionEpic);

export default combineReducers({
  api,
  router: routerReducer,
  session,
  game
});
