import { createAction, handleActions } from 'redux-actions';

export const addQuestion = createAction('game/ADD_QUESTION');
export const nextQuestion = createAction('game/NEXT_QUESTION');
export const toggleWaiting = createAction('game/TOGGLE_WAITING');
export const sessionFinished = createAction('game/SESSION_FINISHED');
export const receiveAnswer = createAction('game/Receive_Answer');
export const setGamePin = createAction('game/SET_GAME_PIN');

export default (...args) => {
  return handleActions(
    {
      [addQuestion](state, { payload }) {
        return {
          ...state,
          questions: payload,
          count: 0,
          answers: [{ answerA: 0, answerB: 0, answerC: 0, answerD: 0 }]
        };
      },
      [nextQuestion](state) {
        return {
          ...state,
          count: state.count + 1,
          questionCount: 0,
          answers: state.answers.concat({
            answerA: 0,
            answerB: 0,
            answerC: 0,
            answerD: 0
          })
        };
      },
      [setGamePin](state, { payload }) {
        return {
          ...state,
          gamePin: payload
        };
      },
      [toggleWaiting](state, { payload }) {
        return { ...state, waiting: payload };
      },
      [receiveAnswer](state, { payload }) {
        return {
          ...state,
          questionCount: state.questionCount + 1,
          answers: [
            ...state.answers.slice(0, state.count),
            {
              ...state.answers[state.count],
              ...(state.answers[state.count][payload.propt] =
                state.answers[state.count][payload.propt] + 1)
            },
            ...state.answers.slice(state.count + 1)
          ]
        };
      },
      [sessionFinished](state, { payload }) {
        return { ...state, sessionFinished: true };
      }
    },
    {
      questions: [],
      answers: [{ answerA: 0, answerB: 0, answerC: 0, answerD: 0 }],
      count: 0,
      questionCount: 0,
      waiting: true,
      sessionFinished: false,
      gamePin: undefined
    }
  )(...args);
};
