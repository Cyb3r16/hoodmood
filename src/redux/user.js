import { createAction } from 'redux-actions';
import { combineEpics } from 'redux-observable';
import * as api from './api';

const USER_PATH = '/user';
const AUTHENTICATION_PATH = '/auth';

export const createUser = createAction('user/CREATE_USER');
export const login = createAction('user/LOGIN');
export const logout = createAction('user/LOGOUT');
export const tryToLogin = createAction('user/TryToLogin');

export const epic = combineEpics(
  action$ =>
    action$
      .ofType(String(createUser))
      .pluck('payload')
      .mergeMap(user =>
        api.post$(
          {
            path: USER_PATH + '/createUser',
            body: user
          },
          obs => obs.pluck('response').map(api.receiveUser)
        )
      ),

  action$ =>
    action$
      .ofType(String(login))
      .pluck('payload')
      .mergeMap(user =>
        api.post$(
          {
            path: AUTHENTICATION_PATH + '/login',
            body: user
          },
          obs => obs.pluck('response').map(api.receiveUser)
        )
      ),
  action$ =>
    action$
      .ofType(String(logout))
      .pluck('payload')
      .mergeMap(user =>
        api.get$(
          {
            path: AUTHENTICATION_PATH + '/logout',
            body: user
          },
          obs => obs.pluck('response').map(api.logoutUser)
        )
      ),
  action$ =>
    action$.ofType(String(tryToLogin)).mergeMap(data =>
      api.get$(
        {
          path: AUTHENTICATION_PATH + '/getLoggedInUser'
        },
        obs => obs.pluck('response').map(api.receiveUser)
      )
    )
);
