import { createAction, handleActions } from 'redux-actions';
import { combineEpics } from 'redux-observable';
import * as api from './api';

const SESSION_PATH = '/session';

export const createSession = createAction('session/CREATE_SESSION');
export const receiveSession = createAction('session/RECEIVE_SESSION');
export const addUser = createAction('session/ADD_USER');
export const checkGamePin = createAction('session/CHECK_GAME_PIN');

export default (...args) => {
  return handleActions(
    {
      [receiveSession](state, { payload }) {
        return { ...state, session: payload };
      },
      [addUser](state, { payload }) {
        return { ...state, user: [payload, ...state.user] };
      }
    },
    {
      session: undefined,
      user: []
    }
  )(...args);
};

export const epic = combineEpics(
  action$ =>
    action$
      .ofType(String(createSession))
      .pluck('payload')
      .mergeMap(session =>
        api.post$(
          {
            path: SESSION_PATH + '/createSession',
            body: session
          },
          obs => obs.pluck('response').map(receiveSession)
        )
      ),
  action$ =>
    action$
      .ofType(String(checkGamePin))
      .pluck('payload')
      .mergeMap(gamePin =>
        api.post$(
          {
            path: SESSION_PATH + '/checkGamePin',
            body: gamePin
          },
          obs => obs.pluck('response').map(receiveSession)
        )
      )
);
